{{/*
Expand the name of the chart.
*/}}
{{- define "dataverse.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "dataverse.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dataverse.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "dataverse.labels" -}}
helm.sh/chart: {{ include "dataverse.chart" . }}
{{ include "dataverse.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "dataverse.selectorLabels" -}}
app.kubernetes.io/name: {{ include "dataverse.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "dataverse.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "dataverse.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "dataverse.postgresql.fullname" -}}
{{- printf "%s-%s" .Release.Name "postgresql" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "dataverse.postgresql.host" -}}
{{- if .Values.postgresql.enabled }}
{{- include "dataverse.postgresql.fullname" . }}
{{- else }}
{{- .Values.externalPostgresqlHost }}
{{- end }}
{{- end -}}

{{- define "dataverse.postgresql.port" -}}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.servicePort | default "5432" }}
{{- else }}
{{- .Values.externalPostgresqlPort | default "5432" }}
{{- end }}
{{- end -}}

{{- define "dataverse.postgresql.database" -}}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.postgresqlDatabase }}
{{- else }}
{{- .Values.externalPostgresql.database | default ( include "dataverse.fullname" . ) }}
{{- end }}
{{- end -}}

{{- define "dataverse.postgresql.username" -}}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.postgresqlUsername }}
{{- else }}
{{- .Values.externalPostgresql.username | default "postgres" }}
{{- end }}
{{- end -}}

{{- define "dataverse.postgresql.password" -}}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.postgresqlPassword }}
{{- else }}
{{- .Values.externalPostgresql.password }}
{{- end }}
{{- end -}}

{{- define "dataverse.solr.fullname" -}}
{{- printf "%s-%s" .Release.Name "solr" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "dataverse.solr.host" -}}
{{- if .Values.solr.enabled }}
{{- include "dataverse.solr.fullname" . }}
{{- else }}
{{- .Values.externalSolrHost }}
{{- end }}
{{- end -}}

{{- define "dataverse.solr.port" -}}
{{- .Values.externalSolrPort | default "8983" }}
{{- end -}}

{{- define "dataverse.solr.collectionName" -}}
{{- if .Values.solr.enabled }}
{{- .Values.solr.collection | default ( include "dataverse.fullname" . ) }}
{{- else }}
{{- .Values.externalSolrCollection | default ( include "dataverse.fullname" . ) }}
{{- end }}
{{- end -}}

{{- define "dataverse.solr.username" -}}
{{- if .Values.solr.enabled }}
{{- .Values.solr.authentication.adminUsername }}
{{- else }}
{{- .Values.externalSolrUser | default ( include "dataverse.fullname" . ) }}
{{- end }}
{{- end -}}

{{- define "dataverse.solr.password" -}}
{{- if .Values.solr.enabled }}
{{- .Values.solr.authentication.adminPassword }}
{{- else }}
{{- .Values.externalSolrPassword }}
{{- end }}
{{- end -}}

{{- define "dataverse.solr.authUrl" -}}
{{- printf "%s:%s@%s:%s" (include "dataverse.solr.username" .) (include "dataverse.solr.password" .) (include "dataverse.solr.host" .) ( include "dataverse.solr.port" .)  -}}
{{- end -}}
